# Login Flow

Projeto de redirecionamento para o flow de autenticação do RHSSO

## Fazer o deploy

Basta informar configurar as variáveis de ambiente:
- SERVER_PORT: variável que expõem a porta onde o serviço estará disponível, valor padrão [8080]
- LOGGING_LEVEL: variável que configura o log level da aplicação, valor padrão [info]
- TARGET_URL: variável onde se informa a url de redirecionamento, valor padrão [http://localhost:8080/auth/realms/test/protocol/openid-connect/auth]
- CLIENT_ID: variável onde se informa o client_id esperado pelo RHSSO

