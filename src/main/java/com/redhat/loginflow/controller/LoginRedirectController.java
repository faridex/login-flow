package com.redhat.loginflow.controller;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * Endpoint que recebe uma requisicao e realiza um redirecionamento para outra url.
 * @author user
 *
 */
@RestController
@RequestMapping("/login")
public class LoginRedirectController {

	Logger log = Logger.getLogger(this.getClass().getName());
	
	@Value("${login.flow.redirect.url.target}")
	private String urlTarget ;
	
	@Value("${login.flow.clientid}")
	private String clientId ;
	


	@GetMapping
	public ResponseEntity<Void> doGetredirect(HttpServletRequest request, HttpServletResponse response) {

		log.info("===== Listando request query param =====");
		String queryParam = extractQueryParam(request);
		log.info(queryParam);
		String redirecUriEncoded = "https://basa_login?"+queryParam;
		try {
			 redirecUriEncoded = URLEncoder.encode(redirecUriEncoded, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			log.info("Nao foi possivel realizar o encoded da query param recebida");
			
		}
		String urlRedirect = urlTarget+"?client_id="+clientId+"&response_type=code&scope=openid&redirect_uri="+redirecUriEncoded;
		return ResponseEntity.status(HttpStatus.FOUND).location(URI.create(urlRedirect)).build();
	}

	private String extractQueryParam(HttpServletRequest request) {
		String queryString = request.getQueryString();
		// if (StringUtils.isNotEmpty(queryString)) {
		// queryString = URLDecoder.decode(queryString,
		// StandardCharsets.UTF_8.toString());
//         String[] parameters = queryString.split("&");
//         for (String parameter : parameters) {
//             String[] keyValuePair = parameter.split("=");
//             System.out.println("Query Param "+keyValuePair[0]+"="+keyValuePair[1]);
//         }
		return queryString;
	}

	private void extractHeader(HttpServletRequest request) {
		Enumeration headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String key = (String) headerNames.nextElement();
			String value = request.getHeader(key);
			System.out.println("Header " + key + ": " + value);

		}
	}

}
