package com.redhat.loginflow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoginFlowApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoginFlowApplication.class, args);
	}

}
